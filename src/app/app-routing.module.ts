import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AdminUiComponent } from './admin-ui/admin-ui.component';
import { SignupComponent } from './signup/signup.component';
import { AuthGuard } from './auth.guard';
import { ProductsComponent } from './products/products.component';
import { EditUserUiComponent } from './edit-user-ui/edit-user-ui.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { UpdateDeliveryStatusComponent } from './update-delivery-status/update-delivery-status.component';

const routes: Routes = [
  {
    path: 'updateDeliveryStatus',
    component: UpdateDeliveryStatusComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orderDetails',
    component: OrderDetailsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'cusDash',
    component: CustomerDashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'products',
    component: ProductsComponent
  },
  {
    path: 'editUserUi',
    component: EditUserUiComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  { 
    path: '', 
    component: AdminUiComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
