import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AlertService } from '../alert.service';
import { Router } from '@angular/router';
import { AppSettings } from '../app-const';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  form: FormGroup;
  baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router
  ) { }
    
  

  ngOnInit() { 
    this.form = this.fb.group({
      full_name: [''],
      user_name: [''],
      _password: ['']
    })
  }

  /*this.form.get('user_name').value
  this.form.get('_password').value
  submitForm(formData: any): void {
    console.log(formData);
  }
*/
 submitForm() {
    // let formData: any = new FormData();
    // formData.append('full_name', this.form.get('full_name').value);
    // formData.append('user_name', this.form.get('user_name').value);
    // formData.append('_password', this.form.get('_password').value);
    // console.log(JSON.stringify(formData));
    this.http.post(this.baseUrl + '/admins/signup', this.form.value).subscribe(
      data => {
        this.alertService.success('Registration successful', true);
        this.router.navigate(['/']);
    },
    error => {
        this.alertService.error('Registration failed! Please fill up all the columns and try again!',error);
    }
    )
  }

}
