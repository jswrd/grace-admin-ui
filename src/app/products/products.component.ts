import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { AlertService } from '../alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSettings } from '../app-const';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: any= [];
  baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(private productService: ProductsService,
    private alertService: AlertService,
    private router: Router) { }

  ngOnInit() {
    this.productService.getProducts().subscribe((data: any[])=>{
      this.products = data;
      console.log(this.products);
    });
  }

  

  deleteProduct(id){
    
    console.log("deleting "+ id);
    this.productService.deleteProduct(id).subscribe(
      res => {
        setTimeout(() => {
          window.location.reload();
        }, 500);
        this.alertService.success('Item has been deleted!', true);
        
        
      },
      err => {
          this.alertService.error('Oops, try again!',err);
          if(err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.router.navigate(['/main'])
            }
          }
      }
    );
  }
  
   


}
