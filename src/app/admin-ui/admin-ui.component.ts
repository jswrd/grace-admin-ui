import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserAuthService } from '../user-auth.service';
import { Router } from '@angular/router';
import { AlertService } from '../alert.service';

@Component({
  selector: 'app-admin-ui',
  templateUrl: './admin-ui.component.html',
  styleUrls: ['./admin-ui.component.css']
})
export class AdminUiComponent implements OnInit {
  form: FormGroup;
  getData: Object = [];
  token: any = []

  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private userAuth: UserAuthService,
    private router: Router,
    private alertService: AlertService
  ) { }
    
  

  ngOnInit() { 
    this.form = this.fb.group({
      user_name: [''],
      _password: ['']
    })
  }

 submitForm() {
   const user_name = this.form.get('user_name').value;
   const _password = this.form.get('_password').value;
  //  console.log(user_name, _password);
   
    this.userAuth.login(user_name, _password).subscribe(data => {
      this.alertService.success('You have been logged in!', true);
      this.token = data['token'];
      localStorage.setItem('token', this.token);
      this.router.navigate(['/main']); 
    },
    error => {
        this.alertService.error('Login failed! Please enter a valid username or password.',error);
    })
  }

}
