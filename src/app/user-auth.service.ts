import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSettings } from './app-const';
@Injectable({
  providedIn: 'root'
})
export class UserAuthService {

  baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient,
    private router: Router,
    ) { }

  login(user_name: any, _password: any) {
    return this.http.post(this.baseUrl + '/admins/login', {
      user_name, _password
    })
  }

  loggedIn() {
    return !!localStorage.getItem('token')
  }

  logOut() {
    localStorage.removeItem('token')
    this.router.navigate(['/'])
  }

  getToken() {
    return localStorage.getItem('token')
  }

}
