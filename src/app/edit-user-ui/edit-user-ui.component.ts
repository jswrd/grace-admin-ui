import { Component, OnInit } from '@angular/core';
import { RandomService } from '../random.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-user-ui',
  templateUrl: './edit-user-ui.component.html',
  styleUrls: ['./edit-user-ui.component.css']
})
export class EditUserUiComponent implements OnInit {

  terms: any;
  privacy: any;
  shipping: any;

  constructor(private edit: RandomService) { }

  ngOnInit() {
    this.edit.getTerms().subscribe(data => {
      this.terms = data;
    });
    this.edit.getPrivacy().subscribe(data => {
      this.privacy = data;
    });
    this.edit.getShipping().subscribe(data => {
      this.shipping = data;
    })
  }

  termsValues(termsForm: NgForm): void {
    const header = termsForm.value.header;
    const para = termsForm.value.para;
    const name = 'terms';
    
    
    this.edit.editUserUi(name, header, para).subscribe(data => {
     console.log(data);
     window.alert('Terms & Conditions updated!!!!');
     setTimeout(() => {
      window.location.reload();
    }, 100)
    },
    error => {
      window.alert('Error!! Please try again.')
    }
    )
  }

  privacyValues(privacyForm: NgForm): void {
    const header = privacyForm.value.header;
    const para = privacyForm.value.para;
    const name = 'privacy';
    
    
    this.edit.editUserUi(name, header, para).subscribe(data => {
     console.log(data);
     window.alert('Terms & Conditions updated!!!!');
     setTimeout(() => {
      window.location.reload();
    }, 100)
    },
    error => {
      window.alert('Error!! Please try again.')
    }
    )
  }

  shipValues(shipForm: NgForm): void {
    const header = shipForm.value.header;
    const para = shipForm.value.para;
    const name = 'shipping';
    
    
    this.edit.editUserUi(name, header, para).subscribe(data => {
     console.log(data);
     window.alert('Terms & Conditions updated!!!!');
     setTimeout(() => {
      window.location.reload();
    }, 100)
    },
    error => {
      window.alert('Error!! Please try again.')
    }
    )
  }

  delete(evt) {
    let id = evt.target.id;
    this.edit.deletePair(id).subscribe(data => {
      console.log(data);
      window.alert('Headers and paragraph deleted!!!');
     setTimeout(() => {
      window.location.reload();
    }, 100)
    })
  }

}
