import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUserUiComponent } from './edit-user-ui.component';

describe('EditUserUiComponent', () => {
  let component: EditUserUiComponent;
  let fixture: ComponentFixture<EditUserUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUserUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
