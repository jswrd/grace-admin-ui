import { Component, OnInit } from '@angular/core';
import { RandomService } from '../random.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-delivery-status',
  templateUrl: './update-delivery-status.component.html',
  styleUrls: ['./update-delivery-status.component.css']
})
export class UpdateDeliveryStatusComponent implements OnInit {

  constructor(private delivery: RandomService, private Router: Router) { }

  ngOnInit() {
  }

  delValues(delForm: NgForm): void {
    const status = delForm.value.status;
    let _id = localStorage.getItem('order_id');
    this.delivery.deliveryStatus(_id, status).subscribe(data => {
     console.log(data);
     window.alert('Delivery status updated!!!');
     this.Router.navigate(['/orderDetails']);
     setTimeout(() => {
      window.location.reload();
    }, 100)
    },
    error => {
      window.alert('Error!! Please try again.')
    }
    )
  }

  confirm() {
    let _id = localStorage.getItem('order_id');
    this.delivery.orderDelivered(_id).subscribe(data => {
      window.alert('Order is delivered!!!');
     this.Router.navigate(['/orderDetails']);
     setTimeout(() => {
      window.location.reload();
    }, 100)
    }
    ,
    error => {
      window.alert('Error!! Please try again.')
    })
  }

}
