import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AlertService } from '../alert.service';
import { Router } from '@angular/router';
import { AppSettings } from '../app-const';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})


export class AddComponent implements OnInit {
  
  selectedProduct = 0;
  selectedSubProduct = 0;
  subProduct = [];

  baseUrl:string = AppSettings.API_ENDPOINT;
 
 
  onSelectProduct(serial: number) {
    this.selectedProduct = serial;
    this.selectedSubProduct = 0;
    this.subProduct = this.getSubProducts().filter((item) => {
      return item.serial === Number(serial)
    });
  }
 
  
 
  getProducts() {
    return [
      { id: 1, name: 'Dental orthodontic' },
      { id: 2, name: 'Dental equipment' },
      { id: 3, name: 'Dental consumables' },
      { id: 4, name: 'Medical consumables' },
      { id: 5, name: 'Medical equipment' },
      { id: 6, name: 'Medical instrument' },
      { id: 7, name: 'Dermatology consumables' },
      { id: 8, name: 'ENT instruments' },
      { id: 9, name: 'Laboratory Devices and consumables' },
      { id: 10, name: 'School lab items and consumables' },
      { id: 11, name: 'Veterinary product' }
    ];
  }
 
  getSubProducts() {
    return [
      { id: 1, serial: 1, name: 'Brackets' },
      { id: 2, serial: 1, name: 'Accesories' },
      { id: 3, serial: 1, name: 'Wires' },
      { id: 4, serial: 1, name: 'Orthodontic instruments' },
      { id: 5, serial: 2, name: 'Dental chair' },
      { id: 6, serial: 2, name: 'Dental X-ray' },
      { id: 7, serial: 2, name: 'Handpieces' },
      { id: 8, serial: 2, name: 'Accesories' },
      { id: 9, serial: 2, name: 'Air compressor & suction' },
      { id: 10, serial: 2, name: 'Sterilization unit' },
      { id: 11, serial: 3, name: 'Dental implant' },
      { id: 12, serial: 3, name: 'Implant instrument' },
    ]
  }
  
  urls = new Array<string>();

  detectFiles(event: { target: { files: any; }; }) {
    this.urls = [];
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        reader.onload = (e: any) => {
          this.urls.push(e.target.result);
        }
        reader.readAsDataURL(file);
      }
    }
  }
 
  form: FormGroup;

  constructor(
    public fb: FormBuilder,
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router
  ) {
   
  }

  ngOnInit() { this.form = this.fb.group({
    mainProduct: [''],
    subProduct: [''],
    item_type: [''],
    item_name: [''],
    item_quantity: [''],
    item_description: [''],
    item_price: [''],
    item_rating: ['']
  }) }

  // uploadFile(event: { target: HTMLInputElement; }) {
  //   const file = (event.target as HTMLInputElement).files[0];
  //   this.form.patchValue({
  //     itemImage: file
  //   });
  //   this.form.get('itemImage').updateValueAndValidity()
  // }
  
    fd = new FormData();
    selectedFile: File = null;
    Headers: string = localStorage.getItem('token');

    onFileSelect(event) {
    this.selectedFile = <File>event.target.files[0];
    this.fd.append("mainProduct", this.form.get('mainProduct').value);
    this.fd.append("subProduct", this.form.get('subProduct').value);
    this.fd.append("item_type", this.form.get('item_type').value);
    this.fd.append("item_name", this.form.get('item_name').value);
    this.fd.append("item_quantity", this.form.get('item_quantity').value);
    this.fd.append("item_description", this.form.get('item_description').value);
    this.fd.append("item_price", this.form.get('item_price').value);
    this.fd.append("item_rating", this.form.get('item_rating').value);
    this.fd.append('file', this.selectedFile, this.selectedFile.name);
    this.fd.append('token', this.Headers)
    }

    submitForm() { 
  
    this.http.post<any>(this.baseUrl + '/products/addItem', this.fd).subscribe(
      res => {
        setTimeout(() => {
          window.location.reload();
        }, 2000);
        this.alertService.success('Item added Successfully!', true);
        
        
      },
      err => {
          this.alertService.error('Error adding Items!',err);
          if(err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.router.navigate(['/main'])
            }
          }
      }
   )
  }
  

  

}