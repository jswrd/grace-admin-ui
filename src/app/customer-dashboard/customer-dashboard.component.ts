import { Component, OnInit } from '@angular/core';
import { RandomService } from '../random.service';

@Component({
  selector: 'app-customer-dashboard',
  templateUrl: './customer-dashboard.component.html',
  styleUrls: ['./customer-dashboard.component.css']
})
export class CustomerDashboardComponent implements OnInit {

  customers: any ;

  constructor(private cus: RandomService) { }

  ngOnInit() {
    this.cus.getCustomers().subscribe(data => {
      this.customers = data;
      console.log(data);
      
    })
  }

  order(evt) {
    let id = evt.target.id;
    localStorage.setItem('id', id);
  }

}
