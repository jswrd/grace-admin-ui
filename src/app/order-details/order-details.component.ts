import { Component, OnInit } from '@angular/core';
import { RandomService } from '../random.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  cancelledOrders: any;
  placedOrders: any;
  deliveredOrders: any;
  refundRequest: any;
  
  constructor(private orders: RandomService) { }

  ngOnInit() {
    console.log(localStorage.getItem('id'));
    let user_id = localStorage.getItem('id');

    this.orders.placedOrderDetails(user_id).subscribe(data => {
      this.placedOrders = data;
      
    });

    this.orders.canceledOrderDetails(user_id).subscribe(data=> {
      this.cancelledOrders = data;
      
    });

    this.orders.deliveredOrderDetails(user_id).subscribe(data=> {
      this.deliveredOrders = data;
      
      

    });

    this.orders.refundRequest(user_id).subscribe(data=> {
      this.refundRequest = data;
      console.log(data);
    })
    
  }

  setOrderId(evt) {
    let order_id = evt.target.id;
    localStorage.setItem('order_id', order_id);
  }

  refund(evt) {
    let order_id = evt.target.id;

    this.orders.refundStatus(order_id).subscribe(data=> {
      window.alert('Refund Status is updated!!');
      setTimeout(() => {
       window.location.reload();
     }, 100)
    })
  }

}
