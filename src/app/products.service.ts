import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppSettings } from './app-const';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient) { }
  
  public getProducts() {
    return this.http.get(this.baseUrl + '/admins/allItems')
  }

  public getImages() {
    return this.http.get(this.baseUrl + '/products/allImages')
  }

  

  deleteProduct(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + `/products/items/${id}`, { responseType: 'text' });
  }

}
