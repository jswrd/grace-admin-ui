import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserAuthService } from './user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
 
  constructor(private _authservice: UserAuthService, 
              private _router: Router) {}

  canActivate() : boolean {
    if (this._authservice.loggedIn()) {
      return true
    } else {
      this._router.navigate(['/'])
      return false
    }
  }

}
