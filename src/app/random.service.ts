import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from './app-const';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  baseUrl:string = AppSettings.API_ENDPOINT;

  constructor(private http: HttpClient) { }

  editUserUi(name: any, header: any, para: any) {
    return this.http.post(this.baseUrl + '/admins/editUserUi', {name, header, para});
  }

  getTerms() {
    return this.http.get(this.baseUrl + '/admins/getTerms');
  }

  getPrivacy() {
    return this.http.get(this.baseUrl + '/admins/getPrivacy');
  }

  getShipping() {
    return this.http.get(this.baseUrl + '/admins/getShipping');
  }

  deletePair(id: any) {
    return this.http.post(this.baseUrl + '/admins/deletePair', {id})
  }

  getCustomers() {
    return this.http.get(this.baseUrl + '/admins/getCustomers')
  }

  placedOrderDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/placedOrderDetails', {user_id});
  }

  canceledOrderDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/cancelledOrderDetails', {user_id});
  }

  deliveredOrderDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/admins/deliveredOrderDetails', {user_id});
  }

  refundRequest(user_id: any) {
    return this.http.post(this.baseUrl + '/admins/getRefund', {user_id});
  }

  deliveryStatus(_id:any, status:any) {
    return this.http.post(this.baseUrl + '/admins/deliveryStatus', {_id, status});
  }

  orderDelivered(_id: any) {
    return this.http.post(this.baseUrl + '/admins/orderDelivered', {_id});
  }

  refundStatus(order_id: any) {
    return this.http.post(this.baseUrl + '/admins/refundStatus', {order_id});
  }

}
