import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MatToolbarModule, MatCardModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import { AddComponent } from './add/add.component';
import { MatInputModule } from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { AdminUiComponent } from './admin-ui/admin-ui.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { SignupComponent } from './signup/signup.component';
import { AlertComponent } from './alert/alert.component';
import { DeleteComponent } from './delete/delete.component';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './token-interceptor.service';
import { ProductsComponent } from './products/products.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { EditUserUiComponent } from './edit-user-ui/edit-user-ui.component';
import { CustomerDashboardComponent } from './customer-dashboard/customer-dashboard.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { UpdateDeliveryStatusComponent } from './update-delivery-status/update-delivery-status.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NavbarComponent,
    AddComponent,
    AdminUiComponent,
    SignupComponent,
    AlertComponent,
    DeleteComponent,
    ProductsComponent,
    EditUserUiComponent,
    CustomerDashboardComponent,
    OrderDetailsComponent,
    UpdateDeliveryStatusComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    MatExpansionModule,
    ReactiveFormsModule,
    MatCardModule,
    MatGridListModule
  ],
  providers: [ AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
